/**
 */

package com.donna;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.apache.cordova.PluginResult.Status;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;

import java.util.Date;

public class CilicoMultiscan extends CordovaPlugin implements OnKeyListener {

    private static final String LOG_TAG = "CilicoMultiscan";
    private CallbackContext scanCallbackContext;

    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);

        scanCallbackContext = null;

        Log.d(LOG_TAG, "Initializing CilicoMultiscan");
    }

    /**
     * Handle Cordova Commands
     *
     * @param action
     * @param args
     * @param callbackContext
     * @return
     * @throws JSONException
     */
    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {

        if (action.equals("echo")) {
            String phrase = args.getString(0);
            // Echo back the first argument
            Log.d(LOG_TAG, phrase);
        }
        else if (action.equals("getDate")) {

            Log.d(LOG_TAG, "getDate -> Scan");

            // Check if the plugin is listening the scan button events
            if( this.scanCallbackContext != null ){

                callbackContext.error("Multiscan listener already running");
                return true;
            }

            // Get the reference to the callbacks and start the listening process
            this.scanCallbackContext= callbackContext;
            this.webView.getView().setOnKeyListener(this);

            // Don't return any result now
            PluginResult pluginResult= new PluginResult(PluginResult.Status.NO_RESULT);
            pluginResult.setKeepCallback(true);
            this.scanCallbackContext.sendPluginResult(pluginResult);

            return true;

        }

        return true;
    }

    /**
     * Stop key listening process when lifecycle is destroyed
     *
     */
    public void onDestroy(){

        // Stop the listening process
        this.webView.getView().setOnKeyListener(null);
    }

    /**
     * Stop key listening when lifecycle resets
     *
     */
    public void onReset(){

        // Stop the listening process
        this.webView.getView().setOnKeyListener(null);
    }

    /**
     * OnKey Listener
     *
     * @param view
     * @param keyCode
     * @param keyEvent
     * @return
     */
    public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {

        // Check if the event is equal to KEY_DOWN
        if( keyEvent.getAction() == KeyEvent.ACTION_DOWN )
        {
            Log.d(LOG_TAG, "OnKeyDown: " + keyCode);

            // Check what button has been pressed
            if( keyCode == KeyEvent.KEYCODE_BRIGHTNESS_DOWN ){

                // Create a new JSONObject with the information and send it
                JSONObject info= new JSONObject();
                try{
                    info.put("signal", new String("scan-right"));
                    sendSignal(info, true);
                    return true;
                }
                catch(JSONException ex){
                    Log.e(LOG_TAG, ex.getMessage());
                }

            }
            else if( keyCode == KeyEvent.KEYCODE_BRIGHTNESS_UP  ){

                // Create a new JSONObject with the information and send it
                JSONObject info= new JSONObject();
                try{
                    info.put("signal", new String("scan-left"));
                    sendSignal(info, true);
                    return true;
                }
                catch(JSONException ex){
                    Log.e(LOG_TAG, ex.getMessage());
                }
            }
        }

        return false;
    }

    /**
     * Send a response back to Javascript
     *
     * @param info
     * @param keepCallback
     */
    private void sendSignal(JSONObject info, boolean keepCallback)
    {
        if( this.scanCallbackContext != null ){

            PluginResult result= new PluginResult(PluginResult.Status.OK, info);
            result.setKeepCallback(keepCallback);
            this.scanCallbackContext.sendPluginResult(result);

        }
    }

}
